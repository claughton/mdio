{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# mdio\n",
    "\n",
    "A library of simple I/O routines for MD trajectory formats.\n",
    "\n",
    "*mdio* is designed to provide basic MD file I/O capabilities. It's not supposed\n",
    "to replace great packages like [mdtraj](www.mdtraj.org) and [mdanalysis](www.mdanalysis.org), but may be useful\n",
    "when all you need is basic MD trajectory file I/O and nothing much more.\n",
    "\n",
    "Currently mdio supports reading and writing dcd, xtc, and Amber netcdf (.nc) format files.\n",
    "\n",
    "## Installation:\n",
    "\n",
    "Easiest via pip:\n",
    "\n",
    "```\n",
    "% pip install mdio\n",
    "```\n",
    "\n",
    "## Usage:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mdio"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To load a trajectory, use *mdio.load()*. This returns an *mdio.Trajectory* object. The format of the trajectory file is detected automatically, without reference to the filename extension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mdio.Trajectory with 10 frames, 892 atoms and box info.\n"
     ]
    }
   ],
   "source": [
    "t = mdio.load('../test/examples/test.nc')\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternative ways of reading files are supported:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = mdio.open('../test/examples/test.nc')\n",
    "t2 = f.read()\n",
    "f.close()\n",
    "print(t2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or using a context manager, and in a frame-by-frame way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with mdio.open('../test/examples/test.dcd') as f:\n",
    "    frames = []\n",
    "    frame = f.read_frame()\n",
    "    while frame is not None:\n",
    "        frames.append(frame)\n",
    "        frame = f.read_frame()\n",
    "t3 = mdio.Trajectory(frames)\n",
    "print(t3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Trajectory files can also be written in a variety of ways. The required format is inferred from the filename extension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a) Using the save() method of a trajectory object:\n",
    "t.save('test.nc')\n",
    "\n",
    "# b) Using mdopen():\n",
    "with mdio.open('test2.dcd', \"w\") as f:\n",
    "    f.write(t)\n",
    "\n",
    "# c) Frame-by-frame:\n",
    "f =  mdio.open('test3.xtc', \"w\")\n",
    "for frame in t.frames():\n",
    "    f.write_frame(frame)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Trajectory objects have three main attributes: the coordinates (a [n_frames, n_atoms, 3] numpy array), the periodic box data (a [n_frames, 3, 3] numpy array, or [None] * n_frames) and the timepoint of the frame (an n_frames-long list of floats).\n",
    "\n",
    "The library stores coordinate and box data in nanometer units (based on ASSUMPTIONS about the units being used in the source trajectory files), and ASSUMES times are in picoseconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(t.xyz), t.xyz.shape, t.xyz[0,0])\n",
    "print(type(t.unitcell_vectors), t.unitcell_vectors.shape, t.unitcell_vectors[0])\n",
    "print(type(t.time), t.time[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Trajectories can be sliced and subsetted:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mdio.Trajectory with 2 frames, 892 atoms and box info.\n",
      "mdio.Trajectory with 1 frames, 892 atoms and box info.\n",
      "mdio.Trajectory with 10 frames, 150 atoms and box info.\n"
     ]
    }
   ],
   "source": [
    "print(t[5:17:3])\n",
    "print(t[3])\n",
    "print(t.select(range(150))) # A new trajectory with just the first 150 atoms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Trajectories can be concatenated or appended to (if they are compatible):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(t[2:4] + t[5:10:2])\n",
    "t2 = t[7:1:-1]\n",
    "t2 += t[:6]\n",
    "print(t2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A few common trajectory analysis/manipulation methods are avalable: RMSD calculation, least-squares fitting, and periodic imaging:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = t.rmsd_from(t[0])\n",
    "print(r[:3])\n",
    "\n",
    "tfit = t.fitted_to(t[1])\n",
    "print(tfit) # Note that once fitted, box info is invalid, so removed.\n",
    "\n",
    "t_imaged = t.packed_around(atom_indices=[222]) # atom 222 is close to a box edge\n",
    "print(t_imaged) # Imaging does not invalidate box info\n",
    "r_packed = t_imaged.rmsd_from(t[0])\n",
    "print(r_packed[:3]) # Imaging has broken up the molecular structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The least-squares fitting and rmsd routines can use weights, but you will need to set these by some external method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "weights = np.zeros(t.n_atoms)\n",
    "weights[:4] = 1.0 # Only use first 4 atoms for the fitting\n",
    "t_weighted_fit = t.fitted_to(t[0], weights)\n",
    "print(t.xyz[3,0] - t_weighted_fit.xyz[3,0]) # small as atom 0 was fitted\n",
    "print(t.xyz[3,100] - t_weighted_fit.xyz[3,100]) # atom 100 was not fitted\n",
    "weighted_rmsds = t.rmsd_from(t[0], weights)\n",
    "print(r[:3]) # rmsd calculated over all atoms (above)\n",
    "print(weighted_rmsds[:3]) # rmsd calculated only over first 4 atoms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in all the above, there is no 'topology' object - for many jobs mdio can manage without one. But you can have one if you want, modelled closely on the MDTraj equivalent:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_with_topology = mdio.load('../test/examples/test.nc', top='../test/examples/test.pdb')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gives you extra options:\n",
    "\n",
    "1. Saving in PDB and GRO formats.\n",
    "2. Using atom selections (modelled on MDTraj syntax)\n",
    "3. Compatibility with nglview (use MDTraj loader)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<mdio.base.Topology object at 0x1094b19e8>\n",
      "[4, 36, 42, 54, 74, 84, 103, 126, 140, 146, 167, 181, 196, 202, 212, 234, 244, 268, 287, 306, 330, 351, 371, 392, 406, 416, 438, 448, 455, 474, 484, 501, 515, 535, 551, 572, 579, 586, 596, 620, 630, 652, 676, 690, 704, 724, 746, 757, 767, 782, 794, 804, 821, 845, 859, 869, 876, 883]\n",
      "[0, 15, 18, 21, 26, 40, 52, 72, 82, 101, 116, 130, 144, 165, 179, 186, 200, 210, 226, 232, 242, 255, 258, 261, 266, 285, 304, 317, 320, 323, 328, 349, 369, 390, 399, 404, 414, 430, 436, 446, 453, 472, 482, 494, 499, 513, 533, 549, 570, 577, 584, 594, 607, 610, 613, 618, 628, 644, 650, 663, 666, 669, 674, 683, 688, 697, 702, 722, 738, 744, 755, 765, 780, 792, 802, 819, 832, 835, 838, 843, 857, 867, 874, 881]\n"
     ]
    }
   ],
   "source": [
    "print(t_with_topology.topology)\n",
    "print(t_with_topology.topology.select('name CA'))\n",
    "print(t_with_topology.topology.select('mass == 14'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c610ccbb141f400a85092ea60b75d79b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "NGLWidget(count=10)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import nglview as nv\n",
    "view = nv.show_mdtraj(t_with_topology)\n",
    "view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
