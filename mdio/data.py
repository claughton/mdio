protein_rnames =  ["ALA", "ASP", "ASN", "ASH", "CYS", 
                   "GLY", "GLU", "GLN", "GLH", "HIS", 
                   "HID", "HIE", "HIP", "LEU", "LYS", 
                   "LYN", "ARG", "ARN", "MET", "PHE", 
                   "PRO", "SER", "THR", "TYR", "VAL", 
                   "ILE", "CYX", "CYH", "TRP"]

nucleic_rnames =   ["ADE", "CYT", "GUA", "THY", 
                   "A", "C", "G", "T", 
                   "DA", "DC", "DG", "DT", 
                   "DA5", "DC5", "DG5", "DT5", 
                   "DA3", "DC3", "DG3", "DT3"]
